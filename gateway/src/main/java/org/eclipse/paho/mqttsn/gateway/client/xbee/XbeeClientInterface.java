package org.eclipse.paho.mqttsn.gateway.client.xbee;

import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;









import org.eclipse.paho.mqttsn.gateway.client.ClientInterface;
import org.eclipse.paho.mqttsn.gateway.core.Dispatcher;
import org.eclipse.paho.mqttsn.gateway.exceptions.MqttsException;
import org.eclipse.paho.mqttsn.gateway.messages.Message;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsAdvertise;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsConnect;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsDisconnect;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsGWInfo;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsMessage;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPingReq;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPingResp;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPubComp;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPubRec;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPubRel;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPuback;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsPublish;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsRegack;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsRegister;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsSearchGW;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsSubscribe;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsUnsubscribe;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsWillMsg;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsWillMsgUpd;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsWillTopic;
import org.eclipse.paho.mqttsn.gateway.messages.mqtts.MqttsWillTopicUpd;
import org.eclipse.paho.mqttsn.gateway.utils.ClientAddress;
import org.eclipse.paho.mqttsn.gateway.utils.GWParameters;
import org.eclipse.paho.mqttsn.gateway.utils.GatewayLogger;









import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.Address;
import pl.agh.mobilne.mqtt.model.Device;
import pl.agh.mobilne.mqtt.model.DeviceConfiguration;
import pl.agh.mobilne.mqtt.model.messages.*;
import pl.agh.mobilne.mqtt.xbee.XbeeDevice;
import pl.edu.agh.mobilne.gdp.algorithm.*;

public class XbeeClientInterface implements ClientInterface {
	public static final String ID = "GW1";
	private static final String PORT = "COM16";
	private static final int BAUD_RATE = 115200;

	private volatile boolean running;
	private Device device = null;
	private Queue<Message> msgQueue = null;
	private Dispatcher dispatcher;

	@Override
	public void initialize() throws MqttsException {
		msgQueue = new ArrayBlockingQueue<Message>(1000);
		try {
			device = new XbeeDevice(new DeviceConfiguration(PORT, BAUD_RATE, ID));
		} catch (DeviceException e) {
			throw new MqttsException(e);
		}
		
		
		// Gateway Discovery Protocol
		
		GatewayNode gn = new GatewayNode(device, XbeeClientInterface.ID);

		
		device.addMessageListener((a, d) -> decodeMsg(d.toByteArray(), a));
		dispatcher = Dispatcher.getInstance();
		
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				gn.broadcastAdverts();
			}
		});
		
		t.start();
		
		
	}

	@Override
	public void sendMsg(ClientAddress address, MqttsMessage msg) {
		if (Arrays.equals(address.getAddress(), Address.BROADCAST.getId().getBytes())) {
			broadcastMsg(msg);
		} else {
			System.out.println("Sending message to client: " + new String(address.getAddress()));
			try {
				device.sendData(new Address(new String(address.getAddress())), msg.toBytes());
			} catch (DeviceCommunicationException | DeviceNotFoundException e) {
				throw new RuntimeException("ClientAddress: " + new String(address.getAddress()) + " message: " + msg);
			}
		}
	}

	@Override
	public void broadcastMsg(MqttsMessage msg) {
		broadcastMsg(0, msg);
	}

	@Override
	public void broadcastMsg(int radius, MqttsMessage msg) {
		try {
			device.sendData(Address.BROADCAST, msg.toBytes());
		} catch (DeviceCommunicationException | DeviceNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void decodeMsg(byte[] data, Address address) {
		MqttsMessage mqttsMsg = null;

		GatewayLogger.log(GatewayLogger.WARN, "XbeeClientInterface decoding message: " + Arrays.toString(data));

		// do some checks for the received packet
		if (data == null) {
			GatewayLogger.log(GatewayLogger.WARN,
					"UDPClientInterface - The received data packet is null. The packet cannot be processed.");
			return;
		}

		if (data.length < GWParameters.getMinMqttsLength()) {
			GatewayLogger.log(GatewayLogger.WARN,
					"UDPClientInterface - Not a valid Mqtts message. The received data packet is too short (length = "
							+ data.length + "). The packet cannot be processed.");
			return;
		}

		if (data.length > GWParameters.getMaxMqttsLength()) {
			GatewayLogger.log(GatewayLogger.WARN,
					"UDPClientInterface - Not a valid Mqtts message. The received data packet is too long (length = "
							+ data.length + "). The packet cannot be processed.");
			return;

		}

		if (data[0] < GWParameters.getMinMqttsLength()) {
			GatewayLogger.log(GatewayLogger.WARN,
					"UDPClientInterface - Not a valid Mqtts message. Field \"Length\" in the received data packet is less than "
							+ GWParameters.getMinMqttsLength() + " . The packet cannot be processed.");
			return;
		}

		if (data[0] != data.length) {

			String additionalMessage = String.format("Actual length: %d, length field: %d, the whole array: %s",
					data.length, data[0], Arrays.toString(data));
			GatewayLogger
					.log(GatewayLogger.WARN,
							"UDPClientInterface - Not a valid Mqtts message. Field \"Length\" in the received data packet does not match the actual length of the packet. The packet cannot be processed. ");
			GatewayLogger.log(GatewayLogger.WARN, additionalMessage);
			return;
		}

		int msgType = (data[1] & 0xFF);
		switch (msgType) {
		case MqttsMessage.ADVERTISE:
			if (data.length != 5) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts ADVERTISE message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsAdvertise(data);
			// TODO Handle this case for load balancing issues
			break;

		case MqttsMessage.SEARCHGW:
			if (data.length != 3) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts SEARCHGW message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsSearchGW(data);
			break;

		case MqttsMessage.GWINFO:
			mqttsMsg = new MqttsGWInfo(data);
			// TODO Handle this case for load balancing issues
			break;

		case MqttsMessage.CONNECT:
			if (data.length < 7) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts CONNECT message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsConnect(data);
			break;

		case MqttsMessage.CONNACK:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.WILLTOPICREQ:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.WILLTOPIC:
			if (data.length < 2) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts WILLTOPIC message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsWillTopic(data);
			break;

		case MqttsMessage.WILLMSGREQ:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.WILLMSG:
			if (data.length < 3) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts WILLMSG message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsWillMsg(data);
			break;

		case MqttsMessage.REGISTER:
			if (data.length < 7) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts REGISTER message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsRegister(data);
			break;

		case MqttsMessage.REGACK:
			if (data.length != 7) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts REGACK message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsRegack(data);
			break;

		case MqttsMessage.PUBLISH:
			if (data.length < 8) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts PUBLISH message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPublish(data);
			break;

		case MqttsMessage.PUBACK:
			if (data.length != 7) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts PUBACK message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPuback(data);
			break;

		case MqttsMessage.PUBCOMP:
			if (data.length != 4) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts PUBCOMP message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubComp(data);
			break;

		case MqttsMessage.PUBREC:
			if (data.length != 4) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts PUBREC message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubRec(data);
			break;

		case MqttsMessage.PUBREL:
			if (data.length != 4) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts PUBREL message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubRel(data);
			break;

		case MqttsMessage.SUBSCRIBE:
			if (data.length < 6) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts SUBSCRIBE message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}

			try {
				mqttsMsg = new MqttsSubscribe(data);
			} catch (MqttsException e) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts SUBSCRIBE message. " + e.getMessage());
				return;
			}
			break;

		case MqttsMessage.SUBACK:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.UNSUBSCRIBE:
			if (data.length < 6) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts UNSUBSCRIBE message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}

			try {
				mqttsMsg = new MqttsUnsubscribe(data);
			} catch (MqttsException e) {
				GatewayLogger.log(GatewayLogger.WARN, "UDPClientInterface - Not a valid Mqtts UNSUBSCRIBE message. "
						+ e.getMessage());
				return;
			}
			break;

		case MqttsMessage.UNSUBACK:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.PINGREQ:
			mqttsMsg = new MqttsPingReq(data);
			break;

		case MqttsMessage.PINGRESP:
			mqttsMsg = new MqttsPingResp(data);
			break;

		case MqttsMessage.DISCONNECT:
			mqttsMsg = new MqttsDisconnect(data);
			break;

		case MqttsMessage.WILLTOPICUPD:
			if (data.length < 2) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts WILLTOPICUPD message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}

			mqttsMsg = new MqttsWillTopicUpd(data);
			break;

		case MqttsMessage.WILLTOPICRESP:
			// we will never receive such a message from the client
			break;

		case MqttsMessage.WILLMSGUPD:
			if (data.length < 3) {
				GatewayLogger.log(GatewayLogger.WARN,
						"UDPClientInterface - Not a valid Mqtts WILLMSGUPD message. Wrong packet length (length = "
								+ data.length + "). The packet cannot be processed.");
				return;
			}

			mqttsMsg = new MqttsWillMsgUpd(data);
			break;

		case MqttsMessage.WILLMSGRESP:
			// we will never receive such a message from the client
			break;

		default:
			GatewayLogger.log(GatewayLogger.WARN, "UDPClientInterface - Mqtts message of unknown type \"" + msgType
					+ "\" received.");
			return;
		}

		// construct an "internal" message and put it to dispatcher's queue
		System.out.println("Before message construction");
		Message msg = new Message(new ClientAddress(address.getId().getBytes()));
		System.out.println("After message construction");
		msg.setType(Message.MQTTS_MSG);
		System.out.println("After setting type");
		msg.setMqttsMessage(mqttsMsg);
		System.out.println("After setting message");
		msg.setClientInterface(this);
		System.out.println("interface type:" + msg.getType());
		this.dispatcher.putMessage(msg);
		System.out.println("interface type- after dispatch:" + msg.getType());
	}

}
