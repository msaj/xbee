package pl.edu.agh.mobilne.gdp.algorithm;

import java.util.Comparator;

import pl.agh.mobilne.mqtt.model.*;

public class GwComparator implements Comparator<Address> {

	@Override
	public int compare(Address gwId1, Address gwId2) {
		if(gwId1.equals(gwId2)) return 0;
		else return -1;
	}

}
