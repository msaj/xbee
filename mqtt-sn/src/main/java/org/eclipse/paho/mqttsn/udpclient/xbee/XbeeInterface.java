/*******************************************************************************
 * Copyright (c) 2010, 2013 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Ian Craggs - initial API and implementation and/or initial documentation
 *******************************************************************************/

package org.eclipse.paho.mqttsn.udpclient.xbee;

import org.eclipse.paho.mqttsn.udpclient.exceptions.MqttsException;
import org.eclipse.paho.mqttsn.udpclient.messages.Message;
import org.eclipse.paho.mqttsn.udpclient.messages.mqttsn.*;
import org.eclipse.paho.mqttsn.udpclient.utils.ClientLogger;
import org.eclipse.paho.mqttsn.udpclient.utils.ClientParameters;
import org.eclipse.paho.mqttsn.udpclient.utils.MsgQueue;
import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.exception.MalformedPacketException;
import pl.agh.mobilne.mqtt.model.Address;
import pl.agh.mobilne.mqtt.model.Device;
import pl.agh.mobilne.mqtt.model.RawMessageListener;
import pl.agh.mobilne.mqtt.model.messages.MessageParser;

public class XbeeInterface implements RawMessageListener {
	
	private final static int MAXUDPSIZE=65536;
	private final static int MINUDPSIZE=2; // assumed to be a "sane" value...
	public final static boolean ENCAPS=false;  //use forwarder encapsulation or not

	private MsgQueue queue;
	private ClientParameters clientParms;
	private Device device;


	public void initialize(MsgQueue queue, ClientParameters clientParms, Device device) throws MqttsException {
		this.device = device;
		try {
			//get the queue
			this.queue = queue;
			this.clientParms = clientParms;
			// set the buffer space.
			if(this.clientParms.getMaxMqttsLength() > MAXUDPSIZE) {
				throw new IllegalArgumentException("Xbee only supports packet sizes up to 64KByte!");
			}
			if(this.clientParms.getMaxMqttsLength() < MINUDPSIZE) {
				throw new IllegalArgumentException("Maximum packet size should be larger than "+MINUDPSIZE);
			}

			device.addRawMessageListener(this);
		} catch (Exception e) {
			throw new MqttsException ("XbeeInterface - Error initializing :" +e);
		}
	}


	public void decodeMsg(byte[] data) {
		MqttsMessage mqttsMsg = null;

		//do some checks for the received packet
		if(data == null) {
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - The received data packet is null. The packet cannot be processed.");
			return;
		}

		if(data.length < clientParms.getMinMqttsLength()) {
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts message. The received data packet is too short (length = "+data.length +"). The packet cannot be processed.");
			return;
		}

		if(data.length > clientParms.getMaxMqttsLength()){
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts message. The received data packet is too long (length = "+data.length +"). The packet cannot be processed.");
			return;

		}

		if((data[0] & 0xFF) < clientParms.getMinMqttsLength()) {
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts message. Field \"Length\" (" + (data[0] & 0xFF) + ") in the received data packet is less than "+clientParms.getMinMqttsLength()+" . The packet cannot be processed.");
			return;
		}

		if((data[0] & 0xFF) != data.length) {
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts message. Field \"Length\" in the received data packet does not match the actual length of the packet. The packet cannot be processed. " + data[0] + ", " + data.length);
			return;
		}


		int msgType = (data[1] & 0xFF);
		switch (msgType) {
		case MqttsMessage.ADVERTISE:
			if(data.length != 5) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts ADVERTISE message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsAdvertise(data);
			break;

		case MqttsMessage.SEARCHGW:
			if(data.length != 3) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts SEARCHGW message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsSearchGW(data);
			break;

		case MqttsMessage.GWINFO:
			mqttsMsg = new MqttsGWInfo(data);
			break;

		case MqttsMessage.CONNECT:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a CONNECT was received, something must be wrong here ...");
			break;

		case MqttsMessage.CONNACK:
			mqttsMsg = new MqttsConnack(data);
			ClientLogger.log(ClientLogger.INFO,	"XbeeInterface - CONNACK received");
			break;

		case MqttsMessage.WILLTOPICREQ:
			mqttsMsg = new MqttsWillTopicReq(data);
			break;

		case MqttsMessage.WILLTOPIC:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a WILLTOPIC was received, something must be wrong here ...");
			break;

		case MqttsMessage.WILLMSGREQ:
			mqttsMsg = new MqttsWillMsgReq(data);
			break;

		case MqttsMessage.WILLMSG:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a WILLMSG was received, something must be wrong here ...");
			break;

		case MqttsMessage.REGISTER:
			if(data.length < 7) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts REGISTER message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsRegister(data);
			break;

		case MqttsMessage.REGACK:
			if(data.length != 7) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts REGACK message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsRegack(data);
			break;

		case MqttsMessage.PUBLISH:
			if(data.length < 8) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts PUBLISH message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPublish(data);
			break;

		case MqttsMessage.PUBACK:
			if(data.length != 7) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts PUBACK message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPuback(data);
			break;

		case MqttsMessage.PUBCOMP:
			if(data.length != 4) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts PUBCOMP message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubComp(data);
			break;

		case MqttsMessage.PUBREC:
			if(data.length != 4) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts PUBREC message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubRec(data);
			break;

		case MqttsMessage.PUBREL:
			if(data.length != 4) {
				ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Not a valid Mqtts PUBREL message. Wrong packet length (length = "+data.length +"). The packet cannot be processed.");
				return;
			}
			mqttsMsg = new MqttsPubRel(data);
			break;

		case MqttsMessage.SUBSCRIBE:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a SUBSCRIBE was received, something must be wrong here ...");
			break;

		case MqttsMessage.SUBACK:
			mqttsMsg = new MqttsSuback(data);
			break;

		case MqttsMessage.UNSUBSCRIBE :
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a UNSUBSCRIBE was received, something must be wrong here ...");
			break;

		case MqttsMessage.UNSUBACK:
			mqttsMsg = new MqttsUnsuback(data);
			break;

		case MqttsMessage.PINGREQ:
			mqttsMsg = new MqttsPingReq(data);
			break;

		case MqttsMessage.PINGRESP:
			mqttsMsg = new MqttsPingResp(data);
			break;

		case MqttsMessage.DISCONNECT :
			mqttsMsg = new MqttsDisconnect(data);
			break;

		case MqttsMessage.WILLTOPICUPD:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a WILLTOPICUPD was received, something must be wrong here ...");
			break;

		case MqttsMessage.WILLTOPICRESP:
			mqttsMsg = new MqttsWillTopicResp(data);
			break;

		case MqttsMessage.WILLMSGUPD:
			//we should never receive such a message from the gateway
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Strange ... a WILLMSGUPD was received, something must be wrong here ...");
			break;

		case MqttsMessage.WILLMSGRESP:
			mqttsMsg = new MqttsWillMsgResp(data);
			break;

		default:
			ClientLogger.log(ClientLogger.WARN, "XbeeInterface - Mqtts message of unknown type \"" + msgType+"\" received.");
			return;
		}

		//put the message to the queue
		Message msg = new Message();
		msg.setType(Message.MQTTS_MSG);
		msg.setMqttsMessage(mqttsMsg);
		this.queue.addLast(msg);
		ClientLogger.log(ClientLogger.INFO, "XbeeInterface - Mqtts message \"" + msgType+"\" put in queue.");
	}

	public void sendMsg(MqttsMessage msg){
		try {
			device.sendData(clientParms.getGatewayAddress(), msg.toBytes());
		} catch (DeviceCommunicationException | DeviceNotFoundException  e) {
			e.printStackTrace();
			ClientLogger.log(ClientLogger.ERROR, "XbeeInterface - Error while writing to the device: " + e.getMessage());
		}
	}

	@Override
	public void onMessage(Address address, byte[] data) {
		ClientLogger.log(ClientLogger.INFO, "XbeeInterface - Packet received, decoding ...");
		decodeMsg(data);
	}
}