package pl.agh.mobilne.mqtt;

import com.digi.xbee.api.utils.HexUtils;

import org.eclipse.paho.mqttsn.udpclient.SimpleMqttsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.agh.mobilne.mqtt.console.MqttsSimpleConsole;
import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.*;
import pl.agh.mobilne.mqtt.model.messages.*;
import pl.agh.mobilne.mqtt.xbee.XbeeDeviceFactory;

import java.util.concurrent.TimeUnit;

import pl.edu.agh.mobilne.gdp.algorithm.*;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final String PORT = "COM17"; // change to physical address used by your device
    private static final int BAUD_RATE = 115200; // speed
    public static final String MY_ID = "C1"; // your device will have this ID
    private static final String GATEWAY_ADDRESS = Address.BROADCAST.getId();; // address of gateway, can be ID, 64-bit address, or 16-bit address

    public static void main(String[] args) {
        Device device = null;
        try {
            device = getDevice();

            // Select your client
            dummyClient(args, device); // dummy client not reading responses, connects to gateway and every five seconds publishes message 'Some data' to predefined topic with id 1
            //simpleClient(args, device); // connects to gateway using PAHO, next every five seconds publishes message 'Some data' to predefined topic with id 1
            //consoleClient(args, device); // interactive console client based on paho MqttsSimpleConsole sample

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            device.close();
        }
    }

    private static void dummyClient(String[] args, Device device) {
        Address target = new Address(GATEWAY_ADDRESS);

        device.addRawMessageListener((address, data) -> System.out.println("Raw message for device 1 for address: " + address + ": " + new String(data)));
        device.addMessageListener((address, message) -> System.out.println("Message for device 1 for address: " + address + ": " + message));
        device.addMessageListener((address, message) -> {
            String msg = "Received message: ";
            msg += " [HEX: " + HexUtils.byteArrayToHexString(message.toByteArray()) + "]";
            if (message instanceof PublishMessage) {
                msg += "Readable: " + new String(((PublishMessage) message).getData());
            }
            System.out.println(msg);
        });
        
        
        // Gateway Discovery Protocol
        
        ClientNode cn = new ClientNode(device, App.MY_ID);
        
		cn.searchGateway();
		
		target = cn.getGW();
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				cn.pingGateway();
			}
		});
		
		t.start();
		
        try {
            device.sendMessage(target, new ConnectMessage(false, false, (short) 50, "Dummy client".getBytes()));
            TimeUnit.SECONDS.sleep(2);
            device.sendMessage(target, new SubscribeMessage(false, Qos.Level_0, TopicType.Predefined, (short) 1, (short) 1));

            short messageId = 2;
            while (true) {
                TimeUnit.SECONDS.sleep(5);
                System.out.println("Sending message");
                device.sendMessage(target, new PublishMessage(false, Qos.Level_0, false, TopicType.Predefined, (short) 1, messageId, "Some data".getBytes()));
                messageId++;
            }
        } catch (DeviceCommunicationException e) {
            e.printStackTrace();
        } catch (DeviceNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static Device getDevice() throws DeviceException {
        XbeeDeviceFactory factory;

        factory = new XbeeDeviceFactory(String.format("{devices:[{port:\"%s\", baudRate: %d, id: \"%s\"}]}", PORT, BAUD_RATE, MY_ID));
        return factory.getDevices().get(0);
    }

    private static void consoleClient(String[] args, Device device) {
        MqttsSimpleConsole.initConsole(args, device);
    }

    private static void simpleClient(String[] args, Device device) throws InterruptedException {
        SimpleMqttsClient client = new SimpleMqttsClient(new Address(GATEWAY_ADDRESS), device);
        client.connect("Mqttsn Client");

        device.addRawMessageListener((address, data) -> System.out.println("Raw message for device 1 for address: " + address + ": " + new String(data)));
        device.addMessageListener((address, message) -> {
            String msg = "Received message: ";
            msg += " [HEX: " + HexUtils.byteArrayToHexString(message.toByteArray()) + "]";
            if (message instanceof PublishMessage) {
                msg += "Readable: " + new String(((PublishMessage) message).getData());
            }
            System.out.println(msg);
        });

        while (true) {
            System.out.println("Sending message");
            client.publish(1, "Some data".getBytes(), false);
            Thread.sleep(5000);
        }
    }
}
