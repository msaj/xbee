package pl.agh.mobilne.mqtt.exception;

public class DeviceCommunicationException extends Exception {
    public DeviceCommunicationException(){
        super("Cannot communicate with device");
    }
}
