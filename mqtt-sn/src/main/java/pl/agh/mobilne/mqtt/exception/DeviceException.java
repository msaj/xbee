package pl.agh.mobilne.mqtt.exception;

public class DeviceException extends Exception{
    public DeviceException(String message, Throwable cause){
        super(message, cause);
    }
}
