package pl.agh.mobilne.mqtt.exception;

public class DeviceNotFoundException extends Exception {
    public DeviceNotFoundException(){
        super("Device cannot be found");
    }
}
