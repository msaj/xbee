package pl.agh.mobilne.mqtt.exception;

public class MalformedPacketException extends Throwable  {
    public MalformedPacketException(String message, Throwable cause){
        super(message, cause);
    }
    public MalformedPacketException(String message){
        super(message);
    }
}
