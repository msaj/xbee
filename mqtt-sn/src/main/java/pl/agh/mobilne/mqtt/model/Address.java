package pl.agh.mobilne.mqtt.model;

import com.digi.xbee.api.models.XBee64BitAddress;

public class Address{
    public static final Address BROADCAST = new Address(XBee64BitAddress.BROADCAST_ADDRESS.toString());

    private String Id;

    public Address(String id) {
        Id = id;
    }

    public String getId() {
        return Id;
    }
    
    public boolean equals(Object o) {
    	if (!(o instanceof Address))
            return false;
        Address address = (Address) o;
        return this.Id.equals(address.getId());
    }
    
    public int hashCode() {
    	
    	return this.Id.hashCode();
    }
}
