package pl.agh.mobilne.mqtt.model;

import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.messages.Message;

import java.util.List;

public interface Device {
    void sendData(Address address, byte[] data) throws DeviceCommunicationException, DeviceNotFoundException;

    void addRawMessageListener(RawMessageListener m);

    void removeRawMessageListener(RawMessageListener m);

    public void close();
    public void addMessageListener(MessageListener listener);

    public void removeMessageListener(MessageListener listener);

    public void sendMessage(Address target, Message message) throws DeviceCommunicationException, DeviceNotFoundException;
    public List<Address> getNeighbours();
}
