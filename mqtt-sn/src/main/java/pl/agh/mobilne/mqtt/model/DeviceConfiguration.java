package pl.agh.mobilne.mqtt.model;

public class DeviceConfiguration{
    private String port;
    private int baudRate;
    private String id;

    public DeviceConfiguration(String port, int baudRate, String id) {
        this.port = port;
        this.baudRate = baudRate;
        this.id = id;
    }
    public String getPort() {
        return port;
    }

    public int getBaudRate() {
        return baudRate;
    }

    public String getId() {
        return id;
    }
}
