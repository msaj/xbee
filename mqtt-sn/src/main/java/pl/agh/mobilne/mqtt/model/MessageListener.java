package pl.agh.mobilne.mqtt.model;

import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.messages.Message;

public interface MessageListener {
    public void onMessage(Address address, Message message) throws DeviceCommunicationException, DeviceNotFoundException, InterruptedException;
}
