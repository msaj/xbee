package pl.agh.mobilne.mqtt.model;

public interface RawMessageListener {
    public void onMessage(Address address, byte[] data);
}
