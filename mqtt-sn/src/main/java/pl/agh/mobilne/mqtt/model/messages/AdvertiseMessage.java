package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class AdvertiseMessage extends Message{
    private byte gatewayId;
    private short duration;

    public byte getGatewayId() {
        return gatewayId;
    }

    public short getDuration() {
        return duration;
    }

    public AdvertiseMessage(byte gatewayId, short duration){
        super(new MessageHeader((byte) 3, MessageType.ADVERTISE));
        this.gatewayId = gatewayId;
        this.duration = duration;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{gatewayId}, BitUtils.shortToBytes(duration));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AdvertiseMessage that = (AdvertiseMessage) o;

        if (duration != that.duration) return false;
        if (gatewayId != that.gatewayId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) gatewayId;
        result = 31 * result + (int) duration;
        return result;
    }
}
