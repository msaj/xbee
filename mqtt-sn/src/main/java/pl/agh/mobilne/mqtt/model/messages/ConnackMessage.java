package pl.agh.mobilne.mqtt.model.messages;

public final class ConnackMessage extends Message {
    private byte returnCode;

    public byte getReturnCode() {
        return returnCode;
    }

    public ConnackMessage(byte returnCode) {
        super(new MessageHeader((byte) 1, MessageType.CONNACK));
        this.returnCode = returnCode;
    }

    @Override
    public byte[] getBody() {
        return new byte[]{returnCode};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ConnackMessage that = (ConnackMessage) o;

        if (returnCode != that.returnCode) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) returnCode;
        return result;
    }
}
