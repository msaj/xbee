package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class ConnectMessage extends Message {
    private final byte flags;
    private final byte protocolId;
    private final short duration;
    private final byte[] clientId;

    public ConnectMessage(byte flags, short duration, byte[] clientId) {
        super(new MessageHeader((byte) (4 + clientId.length), MessageType.CONNECT));
        if(clientId == null || clientId.length > 23 || clientId.length < 1){
            throw new IllegalArgumentException("Invalid clientId. Must be 1-23 characters long");
        }
        this.flags = flags;
        this.protocolId = (byte)1;
        this.duration = duration;
        this.clientId = clientId;
    }

    public ConnectMessage(boolean will, boolean cleanSession, short duration, byte[] clientId){
        this((byte) ((will ? 1<<3 : 0) + (cleanSession ? 1 << 2 : 0)), duration, clientId);
    }

    public boolean isWill(){
        return (((int)flags) & (1 << 3)) > 0;
    }

    public boolean isCleanSession(){
        return (((int)flags) & (1 << 2)) > 0;
    }

    public byte getFlags() {
        return flags;
    }

    public byte getProtocolId() {
        return protocolId;
    }

    public short getDuration() {
        return duration;
    }

    public byte[] getClientId() {
        return clientId;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{flags, protocolId}, BitUtils.shortToBytes(duration), clientId);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ConnectMessage that = (ConnectMessage) o;

        if (duration != that.duration) return false;
        if (flags != that.flags) return false;
        if (protocolId != that.protocolId) return false;
        if (!Arrays.equals(clientId, that.clientId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + (int) protocolId;
        result = 31 * result + (int) duration;
        result = 31 * result + Arrays.hashCode(clientId);
        return result;
    }
}
