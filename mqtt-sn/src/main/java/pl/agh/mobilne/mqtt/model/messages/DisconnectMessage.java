package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class DisconnectMessage extends Message {
    private short duration;
    private boolean hasDuration;

    public DisconnectMessage(short duration) {
        super(new MessageHeader((byte) 2, MessageType.DISCONNECT));
        this.duration = duration;
        hasDuration = true;
    }

    public DisconnectMessage() {
        super(new MessageHeader((byte) 0, MessageType.DISCONNECT));
        hasDuration = false;
    }

    public short getDuration() {
        return duration;
    }

    @Override
    public byte[] getBody() {
        if(hasDuration) {
            return BitUtils.shortToBytes(duration);
        }else{
            return new byte[0];
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DisconnectMessage that = (DisconnectMessage) o;

        if (duration != that.duration) return false;
        if (hasDuration != that.hasDuration) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) duration;
        result = 31 * result + (hasDuration ? 1 : 0);
        return result;
    }
}
