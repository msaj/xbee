package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class EncapsulatedMessage extends Message {
    private final byte control;
    private final byte[] wirelessNodeId;
    private final Message message;

    public EncapsulatedMessage(byte control, byte[] wirelessNodeId, Message message) {
        super(new MessageHeader((byte) (1 + (wirelessNodeId != null ? wirelessNodeId.length : 0)), MessageType.ENCAPSULATED));
        if(wirelessNodeId == null || wirelessNodeId.length == 0){
            throw new IllegalArgumentException("WirelessNodeId cannot be null or empty");
        }
        if(message == null){
            throw new IllegalArgumentException("Message cannot be null");
        }
        this.control = control;
        this.wirelessNodeId = wirelessNodeId;
        this.message = message;
    }

    public byte getControl() {
        return control;
    }

    public byte[] getWirelessNodeId() {
        return wirelessNodeId;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{control}, wirelessNodeId, message.toByteArray());

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EncapsulatedMessage that = (EncapsulatedMessage) o;

        if (control != that.control) return false;
        if (!message.equals(that.message)) return false;
        if (!Arrays.equals(wirelessNodeId, that.wirelessNodeId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) control;
        result = 31 * result + Arrays.hashCode(wirelessNodeId);
        result = 31 * result + message.hashCode();
        return result;
    }
}
