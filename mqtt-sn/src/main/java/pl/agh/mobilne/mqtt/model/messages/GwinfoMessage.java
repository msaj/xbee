package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class GwinfoMessage extends Message {
    private final byte gwId;
    private final byte[] gwAdd;

    public byte getGwId() {
        return gwId;
    }

    public byte[] getGwAdd() {
        return gwAdd;
    }

    public GwinfoMessage(byte gwId, byte[] gwAdd) {
        super(new MessageHeader((byte) (1 + (gwAdd != null ? gwAdd.length : 0)), MessageType.GWINFO));
        this.gwId = gwId;
        this.gwAdd = gwAdd != null ? gwAdd : new byte[0];
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{gwId}, gwAdd);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GwinfoMessage that = (GwinfoMessage) o;

        if (gwId != that.gwId) return false;
        if (!Arrays.equals(gwAdd, that.gwAdd)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) gwId;
        result = 31 * result + (gwAdd != null ? Arrays.hashCode(gwAdd) : 0);
        return result;
    }
}
