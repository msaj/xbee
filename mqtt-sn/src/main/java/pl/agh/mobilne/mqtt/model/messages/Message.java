package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public abstract class Message{
    protected MessageHeader messageHeader;
    public byte[] toByteArray(){
        return BitUtils.concat(messageHeader.toByteArray(), getBody());
    }
    protected Message(MessageHeader messageHeader){
        this.messageHeader = messageHeader;
    }

    public MessageHeader getMessageHeader() {
        return messageHeader;
    }

    public abstract byte[] getBody();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (!messageHeader.equals(message.messageHeader)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return messageHeader.hashCode();
    }
}
