package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class MessageHeader {
    private MessageType type;
    private int length;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageHeader that = (MessageHeader) o;

        if (length != that.length) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + length;
        return result;
    }

    public MessageHeader(int length, MessageType type) {
        if(length > (0xFFFF - 4)){
            throw new IllegalArgumentException("Message cannot be longer than 0xFFFF bytes");
        }
        if(length < 0){
            throw new IllegalArgumentException("Message cannot be shorter than 0x02 bytes");
        }
        this.type = type;
        if(length <= (0xFF-2)) {
            this.length = length + 2;
        }else{
            this.length = length + 4;
        }
    }

    public byte[] toByteArray() {
        byte[] result = null;
        if(length <= 0xFF) {
            result = new byte[2];
            result[0] = (byte)length;
        }else{
            result = new byte[4];
            byte[] lengthInBytes = BitUtils.shortToBytes((short)length);
            result[0] = 0x01;
            result[1] = lengthInBytes[0];
            result[2] = lengthInBytes[1];
        }
        result[result.length - 1] = (byte)type.getTypeFieldValue();

        return result;
    }

    public int getLength() {
        return length;
    }
    public MessageType getType() {
        return type;
    }
}
