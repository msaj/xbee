package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.exception.MalformedPacketException;
import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public class MessageParser {
    private MessageParser(){}

    public static Message parseMessage(byte[] message) throws MalformedPacketException {
        if(message[0] == 0x01){
            return parseMessageWithLongHeader(message);
        }else{
            return parseMessageWithShortHeader(message);
        }
    }

    private static Message parseMessageWithLongHeader(byte[] message) throws MalformedPacketException {
        int length = BitUtils.bytesToInt(new byte[]{0, 0, message[1], message[2]});
        MessageType messageType = Arrays.stream(MessageType.values()).filter(v -> v.getTypeFieldValue() == message[3]).findFirst().get();
        byte[] body = Arrays.copyOfRange(message, 4, message.length);
        return parseMessageBody(new MessageHeader(length - 4, messageType), body);
    }

    private static Message parseMessageBody(MessageHeader messageHeader, byte[] body) throws MalformedPacketException {
        Message result;
        try {
            switch (messageHeader.getType()) {
                case ADVERTISE:
                    result = new AdvertiseMessage(body[0], BitUtils.getShort(body, 1));
                    break;
                case SEARCHGW:
                    result = new SearchgwMessage(body[0]);
                    break;
                case GWINFO:
                    result = new GwinfoMessage(body[0], Arrays.copyOfRange(body, 1, body.length));
                    break;
                case CONNECT:
                    result = new ConnectMessage(body[0], BitUtils.getShort(body, 2), Arrays.copyOfRange(body, 4, body.length));
                    break;
                case CONNACK:
                    result = new ConnackMessage(body[0]);
                    break;
                case WILLTOPICREQ:
                    result = new WilltopicreqMessage();
                    break;
                case WILLTOPIC:
                    if(body.length != 0) {
                        result = new WilltopicMessage(body[0], Arrays.copyOfRange(body, 1, body.length));
                    }else {
                        result = new WilltopicMessage();
                    }
                    break;
                case WILLMSGREQ:
                    result = new WillmsgreqMessage();
                    break;
                case WILLMSG:
                    result = new WillmsgMessage(body);
                    break;
                case REGISTER:
                    result = new RegisterMessage(BitUtils.getShort(body, 0), BitUtils.getShort(body, 2), Arrays.copyOfRange(body, 4, body.length));
                    break;
                case REGACK:
                    result = new RegackMessage(BitUtils.getShort(body, 0), BitUtils.getShort(body, 2), body[4]);
                    break;
                case PUBLISH:
                    result = new PublishMessage(body[0], BitUtils.getShort(body, 1), BitUtils.getShort(body, 3), Arrays.copyOfRange(body, 5, body.length));
                    break;
                case PUBACK:
                    result = new PubackMessage(BitUtils.getShort(body, 0), BitUtils.getShort(body, 2), body[4]);
                    break;
                case PUBCOMP:
                    result = new PubcompMessage(BitUtils.getShort(body, 0));
                    break;
                case PUBREC:
                    result = new PubrecMessage(BitUtils.getShort(body, 0));
                    break;
                case PUBREL:
                    result = new PubrelMessage(BitUtils.getShort(body, 0));
                    break;
                case SUBSCRIBE: {
                    byte flags = body[0];
                    int topicIdType = flags & 0x11;
                    if (topicIdType == 0x01) {
                        result = new SubscribeMessage(flags, BitUtils.getShort(body, 1), BitUtils.getShort(body, 3));
                    } else if (topicIdType == 0x00 || topicIdType == 0x10) {
                        result = new SubscribeMessage(flags, BitUtils.getShort(body, 1), Arrays.copyOfRange(body, 3, body.length));
                    } else {
                        throw new IllegalArgumentException("Cannot recognize TopicIdType");
                    }
                    break;
                }
                case SUBACK:
                    result = new SubackMessage(body[0], BitUtils.getShort(body, 1), BitUtils.getShort(body, 3), body[5]);
                    break;
                case UNSUBSCRIBE: {
                    byte flags = body[0];
                    int topicIdType = flags & 0x11;
                    if (topicIdType == 0x01) {
                        result = new UnsubscribeMessage(flags, BitUtils.getShort(body, 1), BitUtils.getShort(body, 3));
                    } else if (topicIdType == 0x00 || topicIdType == 0x10) {
                        result = new UnsubscribeMessage(flags, BitUtils.getShort(body, 1), Arrays.copyOfRange(body, 3, body.length));
                    } else {
                        throw new IllegalArgumentException("Cannot recognize TopicIdType");
                    }
                    break;
                }
                case UNSUBACK:
                    result = new UnsubackMessage(BitUtils.getShort(body, 0));
                    break;
                case PINGREQ:
                    result = new PingreqMessage(body);
                    break;
                case PINGRESP:
                    result = new PingrespMessage();
                    break;
                case DISCONNECT:
                    if(body.length > 0) {
                        result = new DisconnectMessage(BitUtils.getShort(body, 0));
                    }else{
                        result = new DisconnectMessage();
                    }
                    break;
                case WILLTOPICUPD:
                    result = new WilltopicupdMessage(body[0], Arrays.copyOfRange(body, 1, body.length));
                    break;
                case WILLTOPICRESP:
                    result = new WilltopicrespMessage(body[0]);
                    break;
                case WILLMSGUPD:
                    result = new WillmsgupdMessage(Arrays.copyOf(body, body.length));
                    break;
                case WILLMSGRESP:
                    result = new WillmsgrespMessage(body[0]);
                    break;
                case ENCAPSULATED:
                    int wirelessLength = messageHeader.getLength() - messageHeader.toByteArray().length - 1;
                    result = new EncapsulatedMessage(body[0], Arrays.copyOfRange(body, 1, 1 + wirelessLength),
                            parseMessage(Arrays.copyOfRange(body, 1 + wirelessLength, body.length)));
                    break;
                default:
                    throw new MalformedPacketException("Packet is malformed, unrecognized type: " + messageHeader.getType());
            }
        }catch(Exception e){
            throw new MalformedPacketException("Packet is malformed", e);
        }

        if(messageHeader.getType() != MessageType.ENCAPSULATED){
            int expectedLength = messageHeader.toByteArray().length + body.length;
            int actualLength = result.toByteArray().length;
            if(expectedLength != actualLength){
                throw new MalformedPacketException("Packet is malformed, received body has invalid length. Expected length " +expectedLength + ", actual length: " + actualLength);
            }
        }

        return result;
    }

    private static Message parseMessageWithShortHeader(byte[] message) throws MalformedPacketException {
        int length = BitUtils.bytesToInt(new byte[]{0, 0, 0, message[0]});
        MessageType messageType = Arrays.stream(MessageType.values()).filter(v -> v.getTypeFieldValue() == message[1]).findFirst().get();
        byte[] body = Arrays.copyOfRange(message, 2, message.length);
        return parseMessageBody(new MessageHeader(length - 2, messageType), body);
    }
}
