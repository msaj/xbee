package pl.agh.mobilne.mqtt.model.messages;

public enum MessageType{
    ADVERTISE((byte)0x0),
    SEARCHGW((byte)0x1),
    GWINFO((byte)0x2),
    CONNECT((byte)0x4),
    CONNACK((byte)0x5),
    WILLTOPICREQ((byte)0x6),
    WILLTOPIC((byte)0x7),
    WILLMSGREQ((byte)0x8),
    WILLMSG((byte)0x9),
    REGISTER((byte)0xA),
    REGACK((byte)0xB),
    PUBLISH((byte)0xC),
    PUBACK((byte)0xD),
    PUBCOMP((byte)0xE),
    PUBREC((byte)0xF),
    PUBREL((byte)0x10),
    SUBSCRIBE((byte)0x12),
    SUBACK((byte)0x13),
    UNSUBSCRIBE((byte)0x14),
    UNSUBACK((byte)0x15),
    PINGREQ((byte)0x16),
    PINGRESP((byte)0x17),
    DISCONNECT((byte)0x18),
    WILLTOPICUPD((byte)(byte)0x1A),
    WILLTOPICRESP((byte)(byte)0x1B),
    WILLMSGUPD((byte)(byte)0x1C),
    WILLMSGRESP((byte)(byte)0x1D),
    ENCAPSULATED((byte)(byte)0xFE);

    private byte typeFieldValue;
    MessageType(byte type){
        typeFieldValue = type;
    }

    public byte getTypeFieldValue() {
        return typeFieldValue;
    }
}
