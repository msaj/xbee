package pl.agh.mobilne.mqtt.model.messages;

import java.util.Arrays;

public final class PingreqMessage extends Message {
    private byte[] clientId;

    public PingreqMessage(byte[] clientId) {
        super(new MessageHeader((byte) (0 + (clientId != null ? clientId.length : 0)), MessageType.PINGREQ));
        this.clientId = clientId != null ? clientId : new byte[0];
    }

    public byte[] getClientId() {
        return clientId;
    }

    @Override
    public byte[] getBody() {
        return clientId;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PingreqMessage that = (PingreqMessage) o;

        if (!Arrays.equals(clientId, that.clientId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (clientId != null ? Arrays.hashCode(clientId) : 0);
        return result;
    }
}
