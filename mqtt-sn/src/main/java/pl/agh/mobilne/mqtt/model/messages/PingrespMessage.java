package pl.agh.mobilne.mqtt.model.messages;

public final class PingrespMessage extends Message {
    public PingrespMessage() {
        super(new MessageHeader(0, MessageType.PINGRESP));
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }
}
