package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class PubackMessage extends Message {
    private final short topicId;
    private final short messageId;
    private final byte returnCode;

    public short getTopicId() {
        return topicId;
    }

    public short getMessageId() {
        return messageId;
    }

    public byte getReturnCode() {
        return returnCode;
    }

    public PubackMessage(short topicId, short messageId, byte returnCode) {
        super(new MessageHeader((byte) 5, MessageType.PUBACK));
        this.topicId = topicId;
        this.messageId = messageId;
        this.returnCode = returnCode;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(BitUtils.shortToBytes(topicId), BitUtils.shortToBytes(messageId), new byte[]{returnCode});
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PubackMessage that = (PubackMessage) o;

        if (messageId != that.messageId) return false;
        if (returnCode != that.returnCode) return false;
        if (topicId != that.topicId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) topicId;
        result = 31 * result + (int) messageId;
        result = 31 * result + (int) returnCode;
        return result;
    }
}
