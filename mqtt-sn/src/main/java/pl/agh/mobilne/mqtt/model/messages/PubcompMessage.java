package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public class PubcompMessage extends Message {
    private short messageId;

    public short getMessageId() {
        return messageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PubcompMessage that = (PubcompMessage) o;

        if (messageId != that.messageId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) messageId;
        return result;
    }

    public PubcompMessage(short messageId) {
        super(new MessageHeader((byte) 2, MessageType.PUBCOMP));
        this.messageId = messageId;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.shortToBytes(messageId);
    }
}
