package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class PublishMessage extends Message {
    private final byte flags;
    private final short topicId;
    private final short messageId;
    private final byte[] data;

    public short getTopicId() {
        return topicId;
    }

    public short getMessageId() {
        return messageId;
    }

    public byte[] getData() {
        return data;
    }

    public PublishMessage(byte flags, short topicId, short messageId, byte[] data) {
        super(new MessageHeader((byte) (5 + (data != null ? data.length : 0)), MessageType.PUBLISH));

        this.flags = flags;
        this.topicId = topicId;
        this.messageId = messageId;
        this.data = data;
    }

    public PublishMessage(boolean dup, Qos qos, boolean retain, TopicType topicIdType,  short topicId, short messageId, byte[] data){
        this((byte) ((dup ? 1 << 7 : 0) + (qos.getValue() << 5) + (retain ? 1 << 4 : 0) + topicIdType.getType()), topicId, messageId, data);
        if(topicIdType != TopicType.Predefined && topicIdType != TopicType.Short){
            throw new IllegalArgumentException("TopicIdType must be predefined or short");
        }
    }

    public boolean isDup(){
        return (((int)flags) & (1 << 7)) > 0;
    }

    public boolean isRetain(){
        return (((int)flags) & (1 << 4)) > 0;
    }

    public Qos getQos(){
        return Qos.fromValue(((int)flags) & (0b11 << 5));
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{flags}, BitUtils.shortToBytes(topicId), BitUtils.shortToBytes(messageId), data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PublishMessage that = (PublishMessage) o;

        if (flags != that.flags) return false;
        if (topicId != that.topicId) return false;
        if (messageId != that.messageId) return false;
        return Arrays.equals(data, that.data);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + (int) topicId;
        result = 31 * result + (int) messageId;
        result = 31 * result + (data != null ? Arrays.hashCode(data) : 0);
        return result;
    }
}
