package pl.agh.mobilne.mqtt.model.messages;

public enum Qos {
    Level_0((byte)0),
    Level_1((byte)1),
    Level_2((byte)2),
    Level_3((byte)3);
    private byte value;

    private Qos(byte value){

        this.value = value;
    }


    public byte getValue() {
        return value;
    }

    public static Qos fromValue(int value){
        switch(value){
            case 0: return Level_0;
            case 1: return Level_1;
            case 2: return Level_2;
            case 3: return Level_3;
            default: throw new IllegalArgumentException("value");
        }
    }
}
