package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class RegisterMessage extends Message {
    private final short topicId;
    private final short messageId;
    private final byte[] topicName;

    public short getTopicId() {
        return topicId;
    }

    public short getMessageId() {
        return messageId;
    }

    public byte[] getTopicName() {
        return topicName;
    }

    public RegisterMessage(short topicId, short messageId, byte[] topicName) {
        super(new MessageHeader((byte) (4 + (topicName != null ? topicName.length : 0)), MessageType.REGISTER));
        this.topicId = topicId;
        this.messageId = messageId;
        this.topicName = topicName;
        if(topicName == null || topicName.length == 0){
            throw new IllegalArgumentException("TopicName cannot be null or empty");
        }
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(BitUtils.shortToBytes(topicId), BitUtils.shortToBytes(messageId), topicName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RegisterMessage that = (RegisterMessage) o;

        if (messageId != that.messageId) return false;
        if (topicId != that.topicId) return false;
        if (!Arrays.equals(topicName, that.topicName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) topicId;
        result = 31 * result + (int) messageId;
        result = 31 * result + Arrays.hashCode(topicName);
        return result;
    }
}
