package pl.agh.mobilne.mqtt.model.messages;

public final class SearchgwMessage extends Message {
    private byte radius;

    public byte getRadius() {
        return radius;
    }

    public SearchgwMessage(byte radius) {
        super(new MessageHeader((byte) 1, MessageType.SEARCHGW));
        this.radius = radius;
    }

    @Override
    public byte[] getBody() {
        return new byte[]{radius};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SearchgwMessage that = (SearchgwMessage) o;

        if (radius != that.radius) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) radius;
        return result;
    }
}
