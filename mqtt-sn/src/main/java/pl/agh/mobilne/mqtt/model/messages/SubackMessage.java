package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class SubackMessage extends Message {
    private final byte flags;
    private final short topicId;
    private final short messageId;
    private final byte returnCode;

    public byte getFlags() {
        return flags;
    }

    public short getTopicId() {
        return topicId;
    }

    public short getMessageId() {
        return messageId;
    }

    public byte getReturnCode() {
        return returnCode;
    }

    public SubackMessage(byte flags, short topicId, short messageId, byte returnCode) {
        super(new MessageHeader((byte) 6, MessageType.SUBACK));
        this.flags = flags;
        this.topicId = topicId;
        this.messageId = messageId;
        this.returnCode = returnCode;
    }

    public SubackMessage(Qos qos, short topicId, short messageId, byte returnCode) {
        this((byte) (qos.getValue() << 5), topicId, messageId, returnCode);
    }


    public Qos getQos(){
        return Qos.fromValue(((int)flags) & (0b11 << 5));
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{flags}, BitUtils.shortToBytes(topicId), BitUtils.shortToBytes(messageId), new byte[]{returnCode});
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SubackMessage that = (SubackMessage) o;

        if (flags != that.flags) return false;
        if (messageId != that.messageId) return false;
        if (returnCode != that.returnCode) return false;
        if (topicId != that.topicId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + (int) topicId;
        result = 31 * result + (int) messageId;
        result = 31 * result + (int) returnCode;
        return result;
    }
}
