package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class SubscribeMessage extends Message {
    private final byte flags;
    private final short msgId;
    private short topicId;

    public byte[] getTopicName() {
        return topicName;
    }

    public byte getFlags() {
        return flags;
    }

    public short getMsgId() {
        return msgId;
    }

    public short getTopicId() {
        return topicId;
    }

    private byte[] topicName;

    public SubscribeMessage(byte flags, short msgId, byte[] topicName) {
        super(new MessageHeader((byte) (3 + (topicName != null ? topicName.length : 0)), MessageType.SUBSCRIBE));
        if(topicName == null || topicName.length == 0){
            throw new IllegalArgumentException("TopicName cannot be null or empty");
        }
        this.flags = flags;
        this.msgId = msgId;
        this.topicName = topicName;
    }

    public SubscribeMessage(byte flags, short msgId, short topicId) {
        super(new MessageHeader((byte) (5), MessageType.SUBSCRIBE));
        this.flags = flags;
        this.msgId = msgId;
        this.topicId = topicId;
    }

    public SubscribeMessage(boolean dup, Qos qos, TopicType topicIdType, short msgId, short topicId){
        this((byte) ((dup ? 1 << 7 : 0) + (qos.getValue() << 5) + topicIdType.getType()), msgId, topicId);
        if(topicIdType != TopicType.Predefined && topicIdType != TopicType.Short){
            throw new IllegalArgumentException("TopicIdType must be predefined or short");
        }
    }

    public SubscribeMessage(boolean dup, Qos qos, short msgId, byte[] topicName) {
        this((byte) ((dup ? 1 << 7 : 0) + (qos.getValue() << 5) + TopicType.Normal.getType()), msgId, topicName);
    }


    public Qos getQos(){
        return Qos.fromValue(((int)flags) & (0b11 << 5));
    }

    public boolean isDup(){
        return (((int)flags) & (1 << 7)) > 0;
    }

    public TopicType getTopicIdType(){
        return TopicType.fromType(((int)flags) & (0b11));
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{flags}, BitUtils.shortToBytes(msgId), topicName != null ? topicName : BitUtils.shortToBytes(topicId));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SubscribeMessage that = (SubscribeMessage) o;

        if (flags != that.flags) return false;
        if (msgId != that.msgId) return false;
        if (topicId != that.topicId) return false;
        if (!Arrays.equals(topicName, that.topicName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + (int) msgId;
        result = 31 * result + (int) topicId;
        result = 31 * result + (topicName != null ? Arrays.hashCode(topicName) : 0);
        return result;
    }
}
