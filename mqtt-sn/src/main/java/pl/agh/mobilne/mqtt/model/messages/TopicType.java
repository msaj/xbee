package pl.agh.mobilne.mqtt.model.messages;

public enum TopicType {
    Normal((byte)0),
    Predefined((byte)1),
    Short((byte)2);

    private byte type;

    private TopicType(byte type){
        this.type = type;
    }

    public byte getType() {
        return type;
    }

    public static TopicType fromType(int type){
        switch(type){
            case 0:return Normal;
            case 1: return Predefined;
            case 2: return Short;
            default: throw new IllegalArgumentException("type");
        }
    }
}
