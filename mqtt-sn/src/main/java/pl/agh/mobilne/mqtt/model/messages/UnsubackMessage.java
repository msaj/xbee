package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

public final class UnsubackMessage extends Message {
    private short msgId;

    public short getMsgId() {
        return msgId;
    }

    public UnsubackMessage(short msgId) {
        super(new MessageHeader((byte) 2, MessageType.UNSUBACK));
        this.msgId = msgId;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.shortToBytes(msgId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UnsubackMessage that = (UnsubackMessage) o;

        if (msgId != that.msgId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) msgId;
        return result;
    }
}
