package pl.agh.mobilne.mqtt.model.messages;

import java.util.Arrays;

public final class WillmsgMessage extends Message {
    private byte[] willMsg;

    public byte[] getWillMsg() {
        return willMsg;
    }

    public WillmsgMessage(byte[] willMsg) {
        super(new MessageHeader((byte) (0 + (willMsg != null ? willMsg.length : 0)), MessageType.WILLMSG));
        this.willMsg = willMsg;
        if(willMsg == null || willMsg.length == 0){
            throw new IllegalArgumentException("WillMsg cannot be null or empty");
        }
    }

    @Override
    public byte[] getBody() {
        return willMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WillmsgMessage that = (WillmsgMessage) o;

        if (!Arrays.equals(willMsg, that.willMsg)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(willMsg);
        return result;
    }
}
