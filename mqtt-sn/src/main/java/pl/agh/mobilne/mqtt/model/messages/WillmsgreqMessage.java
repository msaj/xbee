package pl.agh.mobilne.mqtt.model.messages;

public final class WillmsgreqMessage extends Message {
    public WillmsgreqMessage() {
        super(new MessageHeader((byte) 0, MessageType.WILLMSGREQ));
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }
}
