package pl.agh.mobilne.mqtt.model.messages;

public final class WillmsgrespMessage extends Message {
    private byte returnCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WillmsgrespMessage that = (WillmsgrespMessage) o;

        if (returnCode != that.returnCode) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) returnCode;
        return result;
    }

    public WillmsgrespMessage(byte returnCode) {
        super(new MessageHeader((byte) 1, MessageType.WILLMSGRESP));
        this.returnCode = returnCode;
    }

    @Override
    public byte[] getBody() {
        return new byte[]{returnCode};
    }
}
