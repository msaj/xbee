package pl.agh.mobilne.mqtt.model.messages;

import java.util.Arrays;

public final class WillmsgupdMessage extends Message {
    private byte[] willMsg;

    public WillmsgupdMessage(byte[] willMsg) {
        super(new MessageHeader((byte) (0 + (willMsg != null ? willMsg.length : 0)), MessageType.WILLMSGUPD));
        if(willMsg == null || willMsg.length == 0){
            throw new IllegalArgumentException("WillMsg cannot be null or empty");
        }
        this.willMsg = willMsg;
    }

    public byte[] getWillMsg() {
        return willMsg;
    }

    @Override
    public byte[] getBody() {
        return willMsg;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WillmsgupdMessage that = (WillmsgupdMessage) o;

        if (!Arrays.equals(willMsg, that.willMsg)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(willMsg);
        return result;
    }
}
