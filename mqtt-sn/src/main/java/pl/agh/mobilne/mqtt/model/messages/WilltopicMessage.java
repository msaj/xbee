package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class WilltopicMessage extends Message {
    private byte flags;
    private byte[] willTopic;

    public byte getFlags() {
        return flags;
    }

    public byte[] getWillTopic() {
        return willTopic;
    }

    public WilltopicMessage(byte flags, byte[] willTopic) {
        super(new MessageHeader((byte) (1 + (willTopic != null ? willTopic.length : 0)), MessageType.WILLTOPIC));
        if(willTopic == null || willTopic.length == 0){
            throw new IllegalArgumentException("WillTopic cannot be null or empty");
        }
        this.flags = flags;
        this.willTopic = willTopic;
    }

    public WilltopicMessage(Qos qos, boolean retain, byte[] willTopic){
        this((byte) ((qos.getValue() << 5) + (retain ? 1 << 4 : 0)), willTopic);
    }

    public boolean isRetain(){
        return (((int)flags) & (1 << 4)) > 0;
    }

    public Qos getQos(){
        return Qos.fromValue(((int)flags) & (0b11 << 5));
    }

    public WilltopicMessage() {
        super(new MessageHeader((byte) 2, MessageType.WILLTOPIC));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WilltopicMessage that = (WilltopicMessage) o;

        if (flags != that.flags) return false;
        if (!Arrays.equals(willTopic, that.willTopic)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + (willTopic != null ? Arrays.hashCode(willTopic) : 0);
        return result;
    }

    @Override
    public byte[] getBody() {
        if(willTopic != null) {
            return BitUtils.concat(new byte[]{flags}, willTopic);
        }else{
            return new byte[0];
        }
    }
}
