package pl.agh.mobilne.mqtt.model.messages;

public final class WilltopicreqMessage extends Message {
    public WilltopicreqMessage() {
        super(new MessageHeader((byte) 0, MessageType.WILLTOPICREQ));
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }
}
