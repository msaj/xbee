package pl.agh.mobilne.mqtt.model.messages;

import pl.agh.mobilne.mqtt.utils.BitUtils;

import java.util.Arrays;

public final class WilltopicupdMessage extends Message{
    private final byte flags;
    private final byte[] willTopic;

    public byte getFlags() {
        return flags;
    }

    public byte[] getWillTopic() {
        return willTopic;
    }

    public WilltopicupdMessage(byte flags, byte[] willTopic) {
        super(new MessageHeader((byte) (1 + (willTopic != null ? willTopic.length : 0)), MessageType.WILLTOPICUPD));
        if(willTopic == null || willTopic.length == 0){
            throw new IllegalArgumentException("WillTopic cannot be null or empty");
        }
        this.flags = flags;
        this.willTopic = willTopic;
    }

    public WilltopicupdMessage(Qos qos, boolean retain, byte[] willTopic) {
        this((byte) ((qos.getValue() << 5) + (retain ? 1 << 4 : 0)), willTopic);
    }


    public Qos getQos(){
        return Qos.fromValue(((int)flags) & (0b11 << 5));
    }

    public boolean isRetain(){
        return (((int)flags) & (1 << 4)) > 0;
    }

    @Override
    public byte[] getBody() {
        return BitUtils.concat(new byte[]{flags}, willTopic);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WilltopicupdMessage that = (WilltopicupdMessage) o;

        if (flags != that.flags) return false;
        if (!Arrays.equals(willTopic, that.willTopic)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) flags;
        result = 31 * result + Arrays.hashCode(willTopic);
        return result;
    }
}
