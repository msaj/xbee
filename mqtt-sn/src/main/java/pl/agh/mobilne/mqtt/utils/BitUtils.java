package pl.agh.mobilne.mqtt.utils;

import java.nio.ByteBuffer;

public class BitUtils {
    private BitUtils(){}

    public static byte[] intToBytes(int x){
        return ByteBuffer.allocate(4).putInt(x).array();
    }

    public static byte[] shortToBytes(short x){
        return ByteBuffer.allocate(2).putShort(x).array();
    }

    public static int bytesToInt(byte[] array){
        if(array.length != 4){
            throw new IllegalArgumentException("Incorrect number of bytes: expected 4, received: " + array.length);
        }

        return ByteBuffer.wrap(array).getInt();
    }

    public static byte[] concat(byte[] first, byte[] second) {
        byte[] result = new byte[first.length + second.length];
        System.arraycopy(first, 0, result, 0, first.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static byte[] concat(byte[]... arrays) {
        byte[] result = arrays[0];
        for(int i=1;i<arrays.length;++i){
            result = concat(result, arrays[i]);
        }

        return result;
    }

    public static short getShort(byte[] array, int offset){
        return ByteBuffer.wrap(array).getShort(offset);
    }
}
