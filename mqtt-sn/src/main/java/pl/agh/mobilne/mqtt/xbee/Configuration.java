package pl.agh.mobilne.mqtt.xbee;

import java.util.ArrayList;
import java.util.List;

public class Configuration {
    private List<DeviceDescription> devices = new ArrayList<>();

    public List<DeviceDescription> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceDescription> devices) {
        this.devices = devices;
    }
}
