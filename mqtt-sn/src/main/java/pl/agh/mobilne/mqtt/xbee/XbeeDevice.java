package pl.agh.mobilne.mqtt.xbee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.exception.MalformedPacketException;
import pl.agh.mobilne.mqtt.model.Address;
import pl.agh.mobilne.mqtt.model.Device;
import pl.agh.mobilne.mqtt.model.DeviceConfiguration;
import pl.agh.mobilne.mqtt.model.MessageListener;
import pl.agh.mobilne.mqtt.model.RawMessageListener;
import pl.agh.mobilne.mqtt.model.messages.Message;
import pl.agh.mobilne.mqtt.model.messages.MessageParser;

import com.digi.xbee.api.RemoteXBeeDevice;
import com.digi.xbee.api.XBeeDevice;
import com.digi.xbee.api.exceptions.XBeeException;
import com.digi.xbee.api.listeners.IDataReceiveListener;
import com.digi.xbee.api.models.XBeeMessage;

public class XbeeDevice implements Device, IDataReceiveListener {
    private XBeeDevice device;
    private List<RawMessageListener> rawMessageListeners = new ArrayList<>();
    private List<RemoteXBeeDevice> neighbours = Collections.synchronizedList(new ArrayList<>());
    private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1);
    private List<MessageListener> messageListeners = new ArrayList<MessageListener>();
    private boolean hasDiscovered;
    private Object lockObject = new Object();

    private Runnable discovery = new Runnable() {
        @Override
        public void run() {
            synchronized (lockObject) {
                device.getNetwork().startDiscoveryProcess();
                while (device.getNetwork().isDiscoveryRunning()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

                neighbours.addAll(device.getNetwork().getDevices());
                hasDiscovered = true;
            }
        }
    };

    @Override
    public void sendData(Address address, byte[] data) throws DeviceCommunicationException, DeviceNotFoundException {
        try {
            if(address == Address.BROADCAST || Address.BROADCAST.getId().equals(address.getId())){
                device.sendBroadcastData(data);
                return;
            }

            Optional<RemoteXBeeDevice> target = getTargetDevice(address);
            if(target.isPresent()){
                    device.sendData(target.get(), data);
            }else{
                throw new DeviceNotFoundException();
            }
        } catch (XBeeException e) {
            throw new DeviceCommunicationException();
        }
    }

    public void sendMessage(Address target, Message message) throws DeviceCommunicationException, DeviceNotFoundException {
        if(!hasDiscovered){
            synchronized (lockObject){}
        }

        sendData(target, message.toByteArray());
    }

    public void addMessageListener(MessageListener listener){
        messageListeners.add(listener);
    }

    public void removeMessageListener(MessageListener listener){
        messageListeners.remove(listener);
    }

    private Optional<RemoteXBeeDevice> getTargetDevice(Address address) {
        String addressToMatch = address.getId();
        return neighbours.stream().filter(d -> addressToMatch.equals(d.getNodeID()) || addressToMatch.equals(d.get16BitAddress().toString()) || addressToMatch.equals(d.get64BitAddress().toString())).findFirst();
    }

    void handleMessage(Address address, byte[] data){
        Message message = null;
        try {
            message = MessageParser.parseMessage(data);
        } catch (MalformedPacketException e) {
            e.printStackTrace();
        }
        for(RawMessageListener listener : rawMessageListeners){
            listener.onMessage(address, data);
        }

        publishMessage(address, message);
    }

    private void publishMessage(Address address, Message message) {
        for(MessageListener listener : messageListeners){
            try {
                listener.onMessage(address, message);
            } catch (DeviceCommunicationException e) {
                e.printStackTrace();
            } catch (DeviceNotFoundException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addRawMessageListener(RawMessageListener m){
        rawMessageListeners.add(m);
    }
    @Override
    public void removeRawMessageListener(RawMessageListener m) {
        rawMessageListeners.remove(m);
    }

    @Override
    public void close() {
        device.close();
    }

    public XbeeDevice(DeviceConfiguration config) throws DeviceException {
        device = new XBeeDevice(config.getPort(), config.getBaudRate());
        try {
            device.open();
            device.setParameter("AO", new byte[]{0});
            device.setNodeID(config.getId());
            device.addDataListener(this);
        } catch (XBeeException e) {
            throw new DeviceException(e.getMessage(), e);
        }

        executorService.scheduleWithFixedDelay(discovery, 0, 30, TimeUnit.SECONDS);
    }

    @Override
    public void dataReceived(XBeeMessage xBeeMessage) {
        handleMessage(new Address(xBeeMessage.getDevice().getNodeID()), xBeeMessage.getData());
    }

    public List<Address> getNeighbours(){
        if(!hasDiscovered){
            synchronized (lockObject){}
        }

        return neighbours.stream().map(n -> new Address(n.getNodeID())).collect(Collectors.<Address>toList());
    }
}


