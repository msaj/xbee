package pl.agh.mobilne.mqtt.xbee;

import com.google.gson.Gson;
import pl.agh.mobilne.mqtt.exception.DeviceException;
import pl.agh.mobilne.mqtt.model.DeviceConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class XbeeDeviceFactory {
    private List<XbeeDevice> devices = new ArrayList<>();

    public XbeeDeviceFactory(String configuration) throws DeviceException {
        parseConfiguration(configuration);
    }

    private void parseConfiguration(String configuration) throws DeviceException {
        Configuration config = new Gson().fromJson(configuration, Configuration.class);
        for(DeviceDescription description : config.getDevices()){
            System.out.println("Configuring device " + description.getId());
            XbeeDevice device = new XbeeDevice(new DeviceConfiguration(description.getPort(), description.getBaudRate(), description.getId()));
            devices.add(device);
        }
    }

    public List<XbeeDevice> getDevices() {
        return devices.stream().collect(Collectors.toList());
    }
}
