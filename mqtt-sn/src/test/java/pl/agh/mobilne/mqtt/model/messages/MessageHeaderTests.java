package pl.agh.mobilne.mqtt.model.messages;

import org.junit.Test;
import static org.junit.Assert.*;

public class MessageHeaderTests {
    @Test
    public void toByteArray_shortHeader_correctlySerialized(){
        // Arrange
        MessageHeader messageHeader = new MessageHeader(5 - 2, MessageType.GWINFO);
        byte[] expected = new byte[]{0x5, 0x2};

        // Act
        byte[] actual = messageHeader.toByteArray();

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toByteArray_emptyHeader_correctlySerialized(){
        // Arrange
        MessageHeader messageHeader = new MessageHeader(-2, MessageType.GWINFO);
        // Act
        byte[] actual = messageHeader.toByteArray();
    }

    @Test(expected = IllegalArgumentException.class)
    public void toByteArray_tooShortHeader_correctlySerialized(){
        // Arrange
        MessageHeader messageHeader = new MessageHeader(-3, MessageType.GWINFO);
        // Act
        byte[] actual = messageHeader.toByteArray();
    }

    @Test(expected = IllegalArgumentException.class)
    public void toByteArray_tooLongHeader_correctlySerialized(){
        // Arrange
        MessageHeader messageHeader = new MessageHeader(0xFFFF + 1, MessageType.GWINFO);
        // Act
        byte[] actual = messageHeader.toByteArray();
    }

    @Test
    public void toByteArray_longHeader_correctlySerialized(){
        // Arrange
        MessageHeader messageHeader = new MessageHeader(0xDDDD - 4, MessageType.GWINFO);
        byte[] expected = new byte[]{0x1, (byte)0xDD,(byte) 0xDD, 0x2};

        // Act
        byte[] actual = messageHeader.toByteArray();

        // Assert
        assertArrayEquals(expected, actual);
    }
}
