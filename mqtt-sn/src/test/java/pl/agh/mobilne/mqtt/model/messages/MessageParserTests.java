package pl.agh.mobilne.mqtt.model.messages;

import org.junit.Test;
import pl.agh.mobilne.mqtt.exception.MalformedPacketException;
import static org.junit.Assert.*;

public class MessageParserTests {
    private void testMessage(Message expected) throws MalformedPacketException {
        Message actual = MessageParser.parseMessage(expected.toByteArray());
        assertEquals(expected, actual);
    }

    @Test
    public void AdvertiseMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new AdvertiseMessage((byte) 7, (short) 23)
        );
    }

    @Test
    public void SearchgwMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new SearchgwMessage((byte)7)
        );
    }

    @Test
    public void GwinfoMessage_nonEmptyId_messageParsed() throws MalformedPacketException {
        testMessage(
                new GwinfoMessage((byte)7, new byte[]{0x8, 0x9, 0x10})
        );
    }

    @Test
    public void GwinfoMessage_longId_messageParsed() throws MalformedPacketException {
        testMessage(
                new GwinfoMessage((byte) 7, new byte[300])
        );
    }

    @Test
    public void GwinfoMessage_emptyId_messageParsed() throws MalformedPacketException {
        testMessage(
                new GwinfoMessage((byte)7, new byte[0])
        );
    }

    @Test
    public void GwinfoMessage_nullId_messageParsed() throws MalformedPacketException {
        testMessage(
                new GwinfoMessage((byte)7, null)
        );
    }

    @Test
    public void ConnectMessage_correctClientId_messageParsed() throws MalformedPacketException {
        testMessage(
                new ConnectMessage((byte)7, (short)23, new byte[]{0x9, 0xA})
        );
    }

    @Test
    public void ConnackMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new ConnackMessage((byte)7)
        );
    }

    @Test
    public void WilltopicreqMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicreqMessage()
        );
    }

    @Test
    public void WilltopicMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicMessage((byte) 0, new byte[]{5, 4, 3})
        );
    }

    @Test
    public void WilltopicMessage_emptyWillTopic_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicMessage()
        );
    }

    @Test
    public void WillmsgreqMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgreqMessage()
        );
    }

    @Test
    public void WillmsgMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgMessage(new byte[]{0x23})
        );
    }

    @Test
    public void WillmsgMessage_longWillMsg_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgMessage(new byte[300])
        );
    }

    @Test
    public void RegisterMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new RegisterMessage((short)33, (short)31, new byte[]{0x55})
        );
    }

    @Test
    public void RegisterMessage_longTopicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new RegisterMessage((short)33, (short)31, new byte[300])
        );
    }

    @Test
    public void RegackMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new RegackMessage((short)33, (short)31, (byte)5)
        );
    }

    @Test
    public void PublishMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PublishMessage((byte) 0, (short)33, (short)31, new byte[]{0x9, 0x7})
        );
    }

    @Test
    public void PublishMessage_longData_messageParsed() throws MalformedPacketException {
        testMessage(
                new PublishMessage((byte) 0, (short)33, (short)31, new byte[300])
        );
    }

    @Test
    public void PubackMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PubackMessage((short)33, (short)31, (byte)9)
        );
    }

    @Test
    public void PubrecMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PubrecMessage((short)33)
        );
    }

    @Test
    public void PubrelkMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PubrelMessage((short)33)
        );
    }

    @Test
    public void PubcompMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PubcompMessage((short)33)
        );
    }

    @Test
    public void SubscribeMessage_topicId_messageParsed() throws MalformedPacketException {
        testMessage(
                new SubscribeMessage((byte)1, (short)23, (short) 33)
        );
    }

    @Test
    public void SubscribeMessage_topicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new SubscribeMessage((byte)0, (short)23, new byte[]{0x5})
        );
    }

    @Test
    public void SubscribeMessage_shortTopicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new SubscribeMessage((byte)0x10, (short)23, new byte[]{0x5})
        );
    }

    @Test(expected = MalformedPacketException.class)
    public void SubscribeMessage_invalidTopicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new SubscribeMessage((byte)0x11, (short)23, new byte[]{0x5})
        );
    }

    @Test
    public void SubackMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new SubackMessage((byte)0x10, (short)23, (short)44, (byte)2)
        );
    }

    @Test
    public void UnsubscribeMessage_topicId_messageParsed() throws MalformedPacketException {
        testMessage(
                new UnsubscribeMessage((byte)1, (short)23, (short) 33)
        );
    }

    @Test
    public void UnsubscribeMessage_topicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new UnsubscribeMessage((byte)0, (short)23, new byte[]{0x5})
        );
    }

    @Test
    public void UnsubscribeMessage_shortTopicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new UnsubscribeMessage((byte)0x10, (short)23, new byte[]{0x5})
        );
    }

    @Test(expected = MalformedPacketException.class)
    public void UnsubscribeMessage_invalidTopicName_messageParsed() throws MalformedPacketException {
        testMessage(
                new UnsubscribeMessage((byte)0x11, (short)23, new byte[]{0x5})
        );
    }

    @Test
    public void UnsubackMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new UnsubackMessage((short)44)
        );
    }

    @Test
    public void PingreqMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PingreqMessage(new byte[]{23})
        );
    }

    @Test
    public void PingrespMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new PingrespMessage()
        );
    }

    @Test
    public void DisconnectMessage_hasDuration_messageParsed() throws MalformedPacketException {
        testMessage(
                new DisconnectMessage((short)23)
        );
    }

    @Test
    public void DisconnectMessage_noDuration_messageParsed() throws MalformedPacketException {
        testMessage(
                new DisconnectMessage()
        );
    }

    @Test
    public void WilltopicupdMessage_shortWillTopic_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicupdMessage((byte)1, new byte[]{0x33})
        );
    }

    @Test
    public void WilltopicupdMessage_longWillTopic_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicupdMessage((byte)1, new byte[300])
        );
    }

    @Test
    public void WillmsgupdMessage_shortWillTopic_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgupdMessage(new byte[]{0x33})
        );
    }

    @Test
    public void WillmsgupdMessage_longWillTopic_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgupdMessage(new byte[300])
        );
    }

    @Test
    public void WilltopicrespMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WilltopicrespMessage((byte)5)
        );
    }

    @Test
    public void WillmsgrespMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new WillmsgrespMessage((byte)5)
        );
    }

    @Test
    public void EncapsulatedMessage_messageParsed() throws MalformedPacketException {
        testMessage(
                new EncapsulatedMessage((byte)1, new byte[]{0x33, 0x24}, new PingreqMessage(new byte[]{0x35, 0x22}))
        );
    }

    @Test
    public void EncapsulatedMessage_twoLevelOfEncapsulation_messageParsed() throws MalformedPacketException {
        testMessage(
                new EncapsulatedMessage((byte)1, new byte[]{0x55, 0x11, 0x1}, new EncapsulatedMessage((byte)1, new byte[]{0x33, 0x24}, new PingreqMessage(new byte[]{0x35, 0x22})))
        );
    }
}
