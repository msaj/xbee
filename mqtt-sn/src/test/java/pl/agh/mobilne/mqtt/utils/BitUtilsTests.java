package pl.agh.mobilne.mqtt.utils;

import org.junit.Test;
import static org.junit.Assert.*;


public class BitUtilsTests {
    @Test
    public void intToBytes_correctlyExtracted(){
        // Arrange
        int input = 0xFFEEDDCC;
        byte[] expected = new byte[]{ (byte) 0xFF,(byte) 0xEE, (byte) 0xDD, (byte) 0xCC};

        // Act
        byte[] actual = BitUtils.intToBytes(input);

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    public void bytesToInt_correctlyParsed(){
        // Arrange
        int expected = 0xFFEEDDCC;
        byte[] input = new byte[]{ (byte) 0xFF,(byte) 0xEE, (byte) 0xDD, (byte) 0xCC};

        // Act
        int actual = BitUtils.bytesToInt(input);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void shortToBytes_correctlyExtracted(){
        // Arrange
        short input = (short) 0xDDCC;
        byte[] expected = new byte[]{ (byte) 0xDD, (byte) 0xCC};

        // Act
        byte[] actual = BitUtils.shortToBytes(input);

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    public void concat_twoArrays_arraysConcatenated(){
        // Arrange
        byte[] first = new byte[]{(byte)0xDD};
        byte[] second = new byte[]{(byte)0xCC};
        byte[] expected = new byte[]{ (byte) 0xDD, (byte) 0xCC};

        // Act
        byte[] actual = BitUtils.concat(first, second);

        // Assert
        assertArrayEquals(expected, actual);
    }

    @Test
    public void concat_multipleArrays_arraysConcatenated(){
        // Arrange
        byte[] first = new byte[]{(byte)0xDD};
        byte[] second = new byte[]{(byte)0xCC};
        byte[] third = new byte[]{(byte)0xBB};
        byte[] expected = new byte[]{ (byte) 0xDD, (byte) 0xCC, (byte)0xBB};

        // Act
        byte[] actual = BitUtils.concat(first, second, third);

        // Assert
        assertArrayEquals(expected, actual);
    }
}
